from random import choices
from math import sqrt


def main():
    """Главный метод программы"""

    dots_count = 1000
    while True:
        dots = generate_dots(dots_count)
        print(dots)
        idd = choice_dot(dots_count)
        dot = dots[idd]
        print(dot)
        result = get_nearest_dots_count(dots, dot)
        print(result)
        if input('Для продолжения нажмите "Enter" кнопку, для выхода введите "end"') == "end":
            break


def generate_dots(dots_count):
    """Функция генерации координат точек"""

    dots = []
    for i in range(0, dots_count):
        r = range(-500, 500)
        dots.append(choices(r, k=2))
    return dots


def choice_dot(dots_count):
    """Функция выбора точки"""

    r = range(0, dots_count)
    index = choices(r, k=1)
    return index[0]


def get_nearest_dots_count(dots, dot):
    """Функция поиска"""

    find = 0
    for i in range(0, len(dots)):
        x1 = dots[i][0]
        y1 = dots[i][1]
        x0 = dot[0]
        y0 = dot[1]
        x = abs(abs(x1) - abs(x0))
        y = abs(abs(y1) - abs(y0))
        radius = int(30)
        h = sqrt(x ** 2 + y ** 2)
        if h < radius:
            find += 1
    return find


main()
